package kr.ac.bigdatacenter.ftpserver;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PENHCHET on 11/14/2016.
 * Description: Big Data FTP Server
 */
public class BigDataFtpServer {

    private FtpServer ftpServer;

    public void startServer(){

        //TODO: To start the FTP Server
        FtpServerFactory ftpServerFactory = new FtpServerFactory();
        ListenerFactory listenerFactory = new ListenerFactory();
        listenerFactory.setPort(2221);
        ftpServerFactory.addListener("default", listenerFactory.createListener());

        PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();

        //TODO: To create the BaseUser
        BaseUser user = this.createUser();

        //TODO: To create the UserManager
        UserManager userManager = userManagerFactory.createUserManager();

        try{
            //TODO: To save the user to the UserManager
            userManager.save(user);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        //TODO: To set the userManager to the FtpServerFactory
        ftpServerFactory.setUserManager(userManager);
        ftpServer = ftpServerFactory.createServer();
        try{
            //TODO: To start the ftp Server
            System.out.println("STARTING THE BIG DATA FTP SERVER...");
            ftpServer.start();
        }catch(FtpException ex){
            System.out.println("ERROR STARTING THE BIG DATA FTP SERVER...");
            ex.printStackTrace();
        }

    }

    //TODO: To shutdown the FTP Server
    public void shutdown(){
        try {
            //TODO: To shutdown the FTP Server
            System.out.println("SHUTTING DOWN THE BIG DATA FTP SERVER...");
            ftpServer.stop();
        }catch(Exception ex){
            System.out.println("ERROR SHUTTING DOWN THE BIG DATA FTP SERVER...");
            ex.printStackTrace();
        }
    }

    //TODO: To create user information & home directory & authorities
    private BaseUser createUser(){

        //TODO: To create a user with Name = bigdata & Password = Dbnis3258!@#$
        BaseUser user = new BaseUser();
        user.setName("bigdata");
        user.setPassword("Dbnis3258!@#$");
        user.setEnabled(true);
        //TODO: To set the Home Directory for bigdata user
        user.setHomeDirectory("/FTPSERVER");
        List<Authority> authorities = new ArrayList<>();
        authorities.add(new WritePermission());

        //TODO: To set the Authorities
        user.setAuthorities(authorities);
        return user;
    }
}